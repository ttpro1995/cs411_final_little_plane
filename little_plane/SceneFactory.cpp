#include "SceneFactory.h"


SceneFactory::SceneFactory()
{
}

void SceneFactory::createMenuScene(Scene * scene_list, MgList * mg)
{
	ArtWord* startWord = new ArtWord(250, MENU_TOP_Y, 370, 171);
	startWord->setTexture(mg->textureManager->start_artword_texture, 0);
	ArtWord* exitWord = new ArtWord(250, MENU_BOT_Y, 356, 171);
	exitWord->setTexture(mg->textureManager->exit_artword_texture, 0);
	ArtWord* titleWord = new ArtWord(250, 400, 1426, 227);
	titleWord->setTexture(mg->textureManager->title_artword_texture,0);

	MenuChooser* menuChooser = new MenuChooser(150, MENU_TOP_Y, 150, MENU_BOT_Y);
	menuChooser->setTexture(mg->textureManager->chooser_texture, 10);
	menuChooser->setSceneManager(mg->sceneManager);
	menuChooser->setSoundManager(mg->soundManager);

	scene_list[MENU_SCENE].addObj(startWord);
	scene_list[MENU_SCENE].addObj(exitWord);
	scene_list[MENU_SCENE].addObj(menuChooser);
	scene_list[MENU_SCENE].addObj(titleWord);
}

void SceneFactory::createPlayScene(Scene * scene_list, MgList * mg)
{
	PlayerPlane* player = new PlayerPlane();
	player->setTexture(mg->textureManager->play_texture, 10);
	player->setTextureManager(mg->textureManager);
	player->setSoundManager(mg->soundManager);
	player->setSceneManager(mg->sceneManager);
	Spawmer* spawmer = new Spawmer();
	spawmer->setTexture(mg->textureManager->enemy_ufo_texture, 5);
	spawmer->setTextureManager(mg->textureManager);
	spawmer->setSoundManager(mg->soundManager);
	spawmer->setScoreManager(mg->scoreManager);
	ScoreBoard* playScoreBoard = new ScoreBoard(50, 450);
	playScoreBoard->setScoreManager(mg->scoreManager);
	scene_list[PLAY_SCENE].addObj(playScoreBoard);
	scene_list[PLAY_SCENE].addObj(player);
	scene_list[PLAY_SCENE].addObj(spawmer);
}

void SceneFactory::createEndScene(Scene * scene_list, MgList * mg)
{

	ArtWord* retryWord = new ArtWord(250, MENU_TOP_Y, 370, 171);
	retryWord->setTexture(mg->textureManager->retry_artword_texture, 0);
	ArtWord* exitWord = new ArtWord(250, MENU_BOT_Y, 356, 171);
	exitWord->setTexture(mg->textureManager->exit_artword_texture, 0);

	MenuChooser* menuChooser = new MenuChooser(150, MENU_TOP_Y, 150, MENU_BOT_Y);
	menuChooser->setTexture(mg-> textureManager->chooser_texture, 10);
	menuChooser->setSceneManager(mg->sceneManager);
	menuChooser->setSoundManager(mg->soundManager);
	ScoreBoard* endScoreBoard = new ScoreBoard(200, 350);
	endScoreBoard->setScoreManager(mg->scoreManager);

	scene_list[END_SCENE].addObj(retryWord);
	scene_list[END_SCENE].addObj(exitWord);
	scene_list[END_SCENE].addObj(menuChooser);
	scene_list[END_SCENE].addObj(endScoreBoard);
}

