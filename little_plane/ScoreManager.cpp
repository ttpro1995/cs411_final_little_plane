#include "ScoreManager.h"



ScoreManager::ScoreManager()
{
}

void ScoreManager::increase(int value)
{
	score += value;
}

int ScoreManager::getScore()
{
	return score;
}

void ScoreManager::resetScore()
{
	this->score = 0;
}


ScoreManager::~ScoreManager()
{
}
