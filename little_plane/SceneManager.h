#pragma once
class Scene;
class SceneManager;
struct MgList;

// A manager that take care of scene switch
class SceneManager
{
private:
	int scene_code;
	int winID;
	Scene* scene_list;
	MgList* mg;
public:
	 SceneManager();
	 void setSceneList(Scene* scene_list, MgList* mg);
	 void loadScene(int code);
	 int getActiveScene();
	 void setWinID(int winID);
	 void quitApplication();
	 void resetGame();
};

