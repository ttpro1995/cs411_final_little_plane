#pragma once
#include "GameObject.h"
#include "TextureManager.h"
#include"UFO.h"

// a spawmer that spawm a lot of UFO
class Spawmer :
	public GameObject
{
private:
	float last_fire = 0;
	float cooldown = 2;
	Random rand;
public:
	Spawmer();
	~Spawmer();
	GameObject* spawm(int x, int y);

	virtual void update(bool status[]) override;
};

