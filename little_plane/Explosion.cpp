#include "Explosion.h"
#include "Constants.h"
#include"Scene.h"

Explosion::Explosion(int x, int y, int lifetime_frame)
{
	this->lifetime = lifetime_frame;
	this->cur_frame = 0;
	this->x_pos = x;
	this->y_pos = y;
	this->HEIGHT = 60;
	this->WIDTH = 60;
	this->name = "Explosion";
	this->tag = UNTAG;
}


Explosion::~Explosion()
{
}

void Explosion::draw()
{
	refreshTexture();
	glBindTexture(GL_TEXTURE_2D, texture[curTextureIndex -1 ]);
	float x_left = x_pos - WIDTH / 2;
	float x_right = x_pos + WIDTH / 2;
	float y_top = y_pos + HEIGHT / 2;
	float y_bot = y_pos - HEIGHT / 2;
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex2f(x_left, y_top);

	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(x_right, y_top);

	glTexCoord2f(1.0f, 0.0f);
	glVertex2f(x_right, y_bot);

	glTexCoord2f(0.0f, 0.0f);
	glVertex2f(x_left, y_bot);
	glEnd();
}

void Explosion::update(bool status[])
{
	if (this->MARK_FOR_DELETE) {
		// make sure the object is deleted NEXT FRAME
		this->getParent()->removeObj(this);
	}
	cur_frame += 1;
	if (this->cur_frame >= this->lifetime)
		this->MARK_FOR_DELETE = true;

}
