#pragma once
#include "GameObject.h"
#include "SceneManager.h"


// A red dot
// Can choose between start a new game or exit game
class MenuChooser :
	public GameObject
{
private:
	float x_pos1;
	float y_pos1;
	float x_pos2;
	float y_pos2;

	int choose;
	int START = 1;
	int EXIT = 0;
public:
	MenuChooser(float x1, float y1, float x2, float y2);
	~MenuChooser();
	virtual  void draw() override;
	virtual  void update(bool status[]) override;
	void chooseStart();
	void chooseExit();

	virtual  float getXpos() override;
	virtual  float getYpos() override;
	virtual  float getW() override;
	virtual  float getH() override;
};

