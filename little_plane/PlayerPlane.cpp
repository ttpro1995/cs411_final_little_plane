#include "PlayerPlane.h"
#include "Constants.h"
#include "Scene.h"
#include"LaserShot.h"
#include <iostream>
#include<glm\glm.hpp>
#include "SoundManager.h"
#include "Explosion.h"

using namespace std;
PlayerPlane::PlayerPlane()
{
	this->x_pos = 100;
	this->y_pos = 100;
	this->name = "Player Plane";
	this->tag = TAG_PLAYER;
	this->HEIGHT = 60;
	this->WIDTH = 60;
}

void PlayerPlane::draw()
{
	if (loseFlag)
		return;

	refreshTexture();
	glBindTexture(GL_TEXTURE_2D,texture[curTextureIndex]);
	float x_left = x_pos - WIDTH / 2;
	float x_right = x_pos + WIDTH / 2;
	float y_top = y_pos + HEIGHT / 2;
	float y_bot = y_pos - HEIGHT / 2;
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex2f(x_left, y_top);

	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(x_right, y_top);

	glTexCoord2f(1.0f, 0.0f);
	glVertex2f(x_right, y_bot);

	glTexCoord2f(0.0f, 0.0f);
	glVertex2f(x_left, y_bot);
	glEnd();
	//glFlush();
	//cout << "draw plane \n";
}


PlayerPlane::~PlayerPlane()
{
}

void PlayerPlane::update(bool status[])
{
	if (loseCD < 0) {
		this->sceneManager->loadScene(END_SCENE);
		return;
	}
	if (loseFlag) {
		loseCD -= 1;
		return;
	}

	if (status[DIRECTION_LEFT]) {
		movement(DIRECTION_LEFT);
	}
	if (status[DIRECTION_RIGHT]) {
		movement(DIRECTION_RIGHT);
	}
	if (status[DIRECTION_UP]) {
		movement(DIRECTION_UP);
	}
	if (status[DIRECTION_DOWN]) {
		movement(DIRECTION_DOWN);
	}
	if (status[FIRE]) {
		fire();
	}

	//cout << "x = " << this->x_pos << " y = " << this->y_pos << endl;

	draw();
}

void PlayerPlane::movement(int direction)
{
	switch (direction)
	{
	case DIRECTION_UP:
	{
		y_pos += speed;
		break;
	}
	case DIRECTION_DOWN:
	{
		y_pos -= speed;
		break;
	}
	case DIRECTION_LEFT:
	{
		x_pos -= speed;
		break;
	}
	case DIRECTION_RIGHT:
	{
		x_pos += speed;
		break;
	}
	default:
		break;
	}

	// make sure plane cannot go out of screen
	x_pos = glm::clamp(x_pos, bound_x_left, bound_x_right);
	y_pos = glm::clamp(y_pos, bound_y_bot, bound_y_top);
	//cout << "x pos " << x_pos <<" "<<bound_x_left<<" "<<bound_x_right<< endl;
	//cout << "y pos " << y_pos << endl;
}

void PlayerPlane::fire()
{
	clock_t cur = clock();
	clock_t duration = cur - this->last_fire;
	float duration_in_sec = (float)duration / (float)CLOCKS_PER_SEC;


	if (duration_in_sec > cooldown) {
		last_fire = cur;
		LaserShot* shot = new LaserShot(x_pos, y_pos + 20, TAG_PLAYER_ATK);
		shot->setTexture(this->texture, 15);
		Scene* parent = this->getParent();
		parent->addObj(shot);
		soundManager->playMissileLaunch();
	}
}

float PlayerPlane::getXpos()
{
	return this->x_pos;
}

float PlayerPlane::getYpos()
{
	return this->y_pos;
}

float PlayerPlane::getH()
{
	return this->HEIGHT;
}

void PlayerPlane::explosion()
{
	Explosion* expl = new Explosion(this->x_pos, this->y_pos, 10);
	expl->setTexture(this->tm->explosion_texture, 2);
	soundManager->playExplosion();
	this->getParent()->addObj(expl);
}

void PlayerPlane::onCollision(GameObject * other)
{
	if (loseFlag)
		return;
	if (other->tag == TAG_ENEMY || other->tag==TAG_ENEMY_ATK) {
		//TODO: set an explosion
		this->explosion();
		this->loseFlag = true;
		//this->sceneManager->loadScene(END_SCENE);
	}
}

float PlayerPlane::getW()
{
	return (this->WIDTH * 0.8) ;
}