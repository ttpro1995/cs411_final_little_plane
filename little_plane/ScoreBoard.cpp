#include "ScoreBoard.h"
#include <GL\freeglut.h>
#include <string>
#include <iostream>

void ScoreBoard::drawBitmapText(std::string  message, float x, float y, float z)
{
	glDisable(GL_TEXTURE_2D);
	glRasterPos2f(x, y);
	glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (const unsigned char *)message.c_str());
	glEnable(GL_TEXTURE_2D);
}

ScoreBoard::ScoreBoard(int x, int y)
{
	this->x_pos = x;
	this->y_pos = y;
}

void ScoreBoard::draw()
{
	int score = this->scoreManager->getScore();

	drawBitmapText("Score: "+ std::to_string(score), this->x_pos, this->y_pos, 0);
}


ScoreBoard::~ScoreBoard()
{
}
