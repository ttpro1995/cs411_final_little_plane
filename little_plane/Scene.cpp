#include "Scene.h"
#include <iostream>
using namespace std;
#include <iterator>
#include <algorithm>    // std::remove_if
#include "Constants.h"
#include <GL\freeglut.h>
#include <glm\glm.hpp>
using namespace std;
bool Scene::isCollide(GameObject * obs1, GameObject * obs2)
{
	float x1 = obs1->getXpos();
	float y1 = obs1->getYpos();
	float H1 = obs1->getH() / 2;
	float W1 = obs1->getW() / 2;

	float x2 = obs2->getXpos();
	float y2 = obs2->getYpos();
	float H2 = obs2->getH() / 2;
	float W2 = obs2->getW() / 2;

	float absX = glm::abs(x1 - x2);
	float absY = glm::abs(y1 - y2);
	float coef = 0.8;
	float sumH = (H1 + H2) * coef;
	float sumW = (W1 + W2) * coef;
	
	if (H1 == 0 || H2 == 0 || W1 == 0 || H2 == 0)
		return false;

	if (absX < sumW && absY < sumH)
		return true;

	return false;
}
Scene::Scene()
{
	curFrameDuration = 0;
	lastFrameTime = 0;
	resetStatus();
}

void Scene::addTexture(GLuint * texture)
{
	this->texture = texture;
}

void Scene::gameLoop()
{
	clock_t this_time = clock();
	if (lastFrameTime != 0) {
		clock_t duration = this_time - lastFrameTime;
		float duration_in_sec = (float)duration / (float)CLOCKS_PER_SEC;
		curFrameDuration += duration_in_sec;
		
		if (curFrameDuration >= frameDuration)
			this->update();
		lastFrameTime = this_time;
	}
	else {//for the first time
		lastFrameTime = this_time;
	}
}

void Scene::resetStatus()
{
	for (int i = 0; i < MAX_STATUS; i++) {
		status[i] = 0;
	}
}

void Scene::setStatus(int code)
{
	status[code] = true;
}

void Scene::update()
{
	curFrameDuration = 0;
	// cout << "scene update \n";
	runCollisionCheck();

	for (int i = 0; i < objList.size(); i++) {
		objList[i]->update(status);
	}
	glutPostRedisplay();
	resetStatus();
}

void Scene::draw() {
	for (int i = 0; i < objList.size(); i++) {
		objList[i]->draw();
	}
	//cout << "draw scene \n";
}

void Scene::removeObj(GameObject * obj)
{
	std::vector<GameObject*>::iterator pos;
	pos = find(objList.begin(), objList.end(), obj);
	if (pos != objList.end()) {
		GameObject* tmp = *pos;
		objList.erase(pos);
		delete tmp;
		// cout << "remove gameobj success \n";
	}
}

void Scene::cleanScene()
{
	remove_if(this->objList.begin(), this->objList.end(), Scene::deleteAll);
	while (this->objList.size() > 0) {
		this->objList.pop_back();
	}
}

void Scene::runCollisionCheck()
{
	int length = this->objList.size();
	for (int i = 0; i < length; i++)
		for (int j = i + 1; j < length; j++) {
			if (isCollide(this->objList[i], this->objList[j])) {
				this->objList[i]->onCollision(this->objList[j]);
				this->objList[j]->onCollision(this->objList[i]);
			}
		}
}

void Scene::addObj(GameObject* obj)
{
	obj->setParent(this);
	objList.push_back(obj);
}

Scene::~Scene()
{
}
