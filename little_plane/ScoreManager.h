#pragma once

// Keep track of score
class ScoreManager
{
private:
	int score = 0;
public:
	ScoreManager();
	void increase(int value);
	int getScore();
	void resetScore();
	~ScoreManager();
};

