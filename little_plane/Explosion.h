#pragma once
#include "GameObject.h"

// an explosion effect
class Explosion :
	public GameObject
{
private:
	//life time by frame
	int lifetime;
	int cur_frame = 0;
public:
	Explosion(int x, int y, int lifetime_frame);
	~Explosion();
	virtual  void draw() override;
	virtual  void update(bool status[]) override;
};

