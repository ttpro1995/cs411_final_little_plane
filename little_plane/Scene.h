#pragma once
#include <ctime>
#include "GameObject.h"
#include <vector>
 
class Scene
{
private:
	float curFrameDuration;
	float FPS = 30; //FPS = 30 (30 frame per second)
	float frameDuration = 1/FPS; 
	clock_t lastFrameTime = 0; // last frame time
	bool status[100];// an array of switch that turn on/off (check Constants.h file)
	std::vector<GameObject*> objList;// all object in scene
	GLuint* texture;


	// check if 2 object is collide
	bool isCollide(GameObject* obs1, GameObject* obs2);
	
	// check collide every pair of object 
	// call onCollision on both of them if they collide
	void runCollisionCheck();
public:
	Scene();
	void addTexture(GLuint* texture);

	// counter the time
	// put in idle function, it will calculate time to run update()
	void gameLoop(); 
	
	// reset status every update()
	void resetStatus(); 
	
	// when the key is pressed, it will record status for next frame
	void setStatus(int code);

	//update object then render every frame
	void update();
	
	// add a pointer of object to scene
	void addObj(GameObject* obj);

	//draw all object of scene, put it is display function
	void draw();

	// remove an object from scene
	void removeObj(GameObject* obj);

	//delete all object on scene
	void cleanScene();

	// a helper function
	static bool deleteAll(GameObject * theElement) 
		{ 
			delete theElement;
			return true; 
		}

	// destructor, nevermind, don't touch or error
	~Scene();
};

