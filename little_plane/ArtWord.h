#pragma once
#include "GameObject.h"


// show sprite only, for UI
// it is "Start" "Retry" "Exit"
class ArtWord:
	public GameObject
{
private:
	float x_pos;
	float y_pos;
	float HEIGHT;
	float WIDTH;
	
public:
	ArtWord(int x, int y, int W, int H);
	~ArtWord();
	virtual  void draw() override;
	virtual  void update(bool status[]) override;
	virtual float getXpos() override;
	virtual float getYpos() override;
	virtual float getH() override;
	virtual float getW() override;
};

