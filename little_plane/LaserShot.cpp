#include "LaserShot.h"
#include <GL\freeglut.h>
#include "Scene.h"
LaserShot::LaserShot(int x, int y, int tag)
{
	this->x_pos = x;
	this->y_pos = y;
	this->HEIGHT = 20;
	this->WIDTH = 20;
	this->name = "Laser";
	this->tag = tag;
	if (tag == TAG_PLAYER_ATK)
		speed = 10;
	else if (tag == TAG_ENEMY_ATK)
		speed = -5;
}


LaserShot::~LaserShot()
{
}

void LaserShot::draw()
{
	refreshTexture();
	glBindTexture(GL_TEXTURE_2D, texture[curTextureIndex+3]);
	float x_left = x_pos - WIDTH / 2;
	float x_right = x_pos + WIDTH / 2;
	float y_top = y_pos + HEIGHT / 2;
	float y_bot = y_pos - HEIGHT / 2;
	if (tag == TAG_PLAYER_ATK) {
		// render it upward when it is shoot from player
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(x_left, y_top);

		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(x_right, y_top);

		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(x_right, y_bot);

		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(x_left, y_bot);
		glEnd();
	}
	else if (tag == TAG_ENEMY_ATK) {
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(x_left, y_top);

		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(x_right, y_top);

		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(x_right, y_bot);

		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(x_left, y_bot);
		glEnd();
	}

	
}

void LaserShot::update(bool status[])
{
	if (this->MARK_FOR_DELETE) {
		// make sure the object is deleted NEXT FRAME
		this->getParent()->removeObj(this);
	}
	y_pos += speed;
	if (y_pos > bound_y_top) {
		// prevent memory leak, delete a bullet when it go out of Screen
		this->MARK_FOR_DELETE = true;
	}
	if (y_pos < bound_y_bot) {
		// prevent memory leak, delete a bullet when it go out of Screen
		this->MARK_FOR_DELETE = true;
	}
}


float LaserShot::getXpos()
{
	return this->x_pos;
}

float LaserShot::getYpos()
{
return this->y_pos;
}

float LaserShot::getH()
{
return this->HEIGHT;
}

void LaserShot::onCollision(GameObject * other)
{
	if (this->tag == TAG_PLAYER_ATK && other->tag == TAG_ENEMY) {
		this->MARK_FOR_DELETE = true;
	}
	else if (this->tag == TAG_ENEMY_ATK && other->tag == TAG_PLAYER) {
		this->MARK_FOR_DELETE = true;
	}
}

float LaserShot::getW()
{
return this->WIDTH;
}
