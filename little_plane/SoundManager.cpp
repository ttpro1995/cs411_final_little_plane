#include "SoundManager.h"
#include <iostream>

using namespace std;


SoundManager::SoundManager()
{
	
	missile_launch_buff.loadFromFile("sound/missile_launch2.wav");
	blip_buff.loadFromFile("sound/Blip_Select.wav");
	blip_sound.setBuffer(blip_buff);
	explosion_buff.loadFromFile("sound/ufo_explosion.wav");
	explosion_sound.setBuffer(explosion_buff);
	select_buff.loadFromFile("sound/select_option.wav");
	select_sound.setBuffer(select_buff);
	//playscene_background_buff.loadFromFile("sound/playscene_background.wav");
}

void SoundManager::playMissileLaunch()
{
	fire_sound.setVolume(100);
	fire_sound.setBuffer(missile_launch_buff);
	fire_sound.play();
	cout << "Missile launch sound \n";
}

void SoundManager::playExplosion()
{
	explosion_sound.setVolume(100);
	explosion_sound.play();
}

void SoundManager::playBlip()
{
	blip_sound.setVolume(100);
	blip_sound.play();
}

void SoundManager::playSelect()
{
	select_sound.setVolume(100);
	select_sound.play();
}

void SoundManager::playPlaySceneBackground()
{
	fire_sound.setVolume(100);
	playscene_background_sound.setBuffer(playscene_background_buff);
	playscene_background_sound.setLoop(true);
	playscene_background_sound.play();
}

void SoundManager::stopPlaySceneBackground()
{
	playscene_background_sound.stop();
}


