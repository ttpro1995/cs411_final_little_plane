#pragma once
#include <GL\freeglut.h>
class Scene;
#include "SoundManager.h"
#include "SceneManager.h"
#include "TextureManager.h"
#include "ScoreManager.h"
#include <string>
class GameObject
{
	/*
	Every game object is player plane, enemy plane must be derived from GameObject class
	*/
private:
	Scene * parentScene;
	int texture_refresh_rate = 0;
	int texture_refresh_counter = 0;

protected:


	TextureManager* tm;
	GLuint* texture;
	SoundManager* soundManager;
	SceneManager* sceneManager;
	ScoreManager* scoreManager;
	int curTextureIndex = 1;

	// if mark is true, an game object will be delete from scene in next frame
	bool MARK_FOR_DELETE = false;

public:
	float x_pos ;
	float y_pos ;
	float HEIGHT = 0;
	float WIDTH = 0;
	std::string name = "no_name"; // name for object (debug purpose)
	int tag = 0; // tag of object, for check collision 

	//set texture to render the game object
	void setTexture(GLuint* texture, int refresh_rate);

	// some manager for calling
	void setSoundManager(SoundManager* sm);
	void setSceneManager(SceneManager* sm);
	void setTextureManager(TextureManager*tm);
	void setScoreManager(ScoreManager* sm);

	// refresh the sprite, allowed multi-sprite each object
	void refreshTexture();

	// the logic function which is call each frame
	// the Scene will call update() of all object it contain
	virtual void update(bool status[]);

	// draw an object 
	virtual void draw();

	// set the reference of Scene to GameObject 
	void setParent(Scene* parentScene);

	Scene* getParent();

	// when 2 object is collided, onCollision is called
	// other is another object which collided with this object
	virtual void onCollision(GameObject* other);

	virtual float getXpos();
	virtual float getYpos();
	virtual float getH();
	virtual float getW();
};

