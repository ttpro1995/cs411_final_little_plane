#include "SceneManager.h"
#include <GL\freeglut.h>
#include"Constants.h"
#include "Scene.h"
#include "SceneFactory.h"
SceneManager::SceneManager()
{
	scene_code = MENU_SCENE;
}
void SceneManager::setSceneList(Scene * scene_list, MgList * mg)
{
	this->scene_list = scene_list;
	this->mg = mg;
}
void SceneManager::loadScene(int code)
{
	scene_code = code;
}

int SceneManager::getActiveScene()
{
	return scene_code;
}

void SceneManager::setWinID(int winID)
{
	this->winID = winID;
}

void SceneManager::quitApplication()
{
	exit(0);
}

void SceneManager::resetGame()
{
	scene_list[PLAY_SCENE].cleanScene();
	mg->scoreManager->resetScore();
	SceneFactory::createPlayScene(scene_list, mg);
}
