#include "GameObject.h"
#include <iostream>
using namespace std;
#include "TextureManager.h"

void GameObject::setTexture(GLuint * texture, int refresh_rate)
{
	this->texture = texture;
	this->texture_refresh_rate = refresh_rate;
}

void GameObject::setSoundManager(SoundManager * sm)
{
	this->soundManager = sm;
}

void GameObject::setSceneManager(SceneManager * sm)
{
	this->sceneManager = sm;
}

void GameObject::setTextureManager(TextureManager * tm)
{
	this->tm = tm;
}

void GameObject::setScoreManager(ScoreManager * sm)
{
	this->scoreManager = sm;
}

void GameObject::refreshTexture()
{
	if (texture_refresh_rate == 0)
		return;
	texture_refresh_counter += 1;
	if (texture_refresh_counter < texture_refresh_rate * 1)
	{
		curTextureIndex = 1;
		return;
	}
	if (texture_refresh_counter < texture_refresh_rate * 2)
	{
		curTextureIndex = 2;
		return;
	}
	if (texture_refresh_counter < texture_refresh_rate * 3)
	{
		curTextureIndex = 3;
		return;
	}
	texture_refresh_counter = 0;
	curTextureIndex = 1;
}

void GameObject::update(bool status[])
{

}

void GameObject::draw()
{

}

void GameObject::setParent(Scene * parentScene)
{
	this->parentScene = parentScene;
}

Scene * GameObject::getParent()
{
	return parentScene;
}

void GameObject::onCollision(GameObject * other)
{
	cout << "collision between " << this->name << " " << other->name << endl;
}

float GameObject::getXpos()
{
	return this->x_pos;
}

float GameObject::getYpos()
{
	return this->y_pos;
}

float GameObject::getH()
{
	return this->HEIGHT;
}

float GameObject::getW()
{
	return this->WIDTH;
}

