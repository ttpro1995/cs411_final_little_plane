#pragma once
#include "GameObject.h"
#include <string>

// Display score on screen
class ScoreBoard :
	public GameObject
{
private:
	void drawBitmapText(std::string  message, float x, float y, float z);
public:
	ScoreBoard(int x,int y);
	virtual  void draw() override;
	~ScoreBoard();
};

