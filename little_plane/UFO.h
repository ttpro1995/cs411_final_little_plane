#pragma once
#include "GameObject.h"
#include "Constants.h"
#include "Random.h"

// a UFO
// that fire a missile every 2 second
class UFO :
	public GameObject
{
private:
	// pos should be center 
	float x_pos;
	float y_pos;
	float HEIGHT = 60;
	float WIDTH = 60;
	float speed = 1;
	int cooldown = 2;
	int scoreValue = 7;
	int last_fire = 0;
	// set up a boundary 
	float bound_x_left = WIDTH / 2;
	float bound_x_right = windows_width - WIDTH / 2;
	float bound_y_top = windows_height - HEIGHT / 2;
	float bound_y_bot = 0 + HEIGHT;
	Random rand;
public:
	UFO(int x, int y);
	~UFO();
	void explosion();
	virtual  void draw() override;
	virtual  void update(bool status[]) override;
	void fire();

	virtual  float getXpos() override;
	virtual  float getYpos() override;
	virtual  float getW() override;
	virtual  float getH() override;

	virtual void onCollision(GameObject* other) override;
};

