#include "UFO.h"
#include "Scene.h"
#include "Explosion.h"
#include "LaserShot.h"

UFO::UFO(int x, int y)
{
	this->x_pos = x;
	this->y_pos = y;
	this->name = "UFO";
	this->tag = TAG_ENEMY;
}


UFO::~UFO()
{
}


// Create visual and audio effect of explosion
// increase score
void UFO::explosion()
{
	Explosion* expl = new Explosion(this->x_pos, this->y_pos, 10);
	expl->setTexture(this->tm->explosion_texture, 2);
	soundManager->playExplosion();
	scoreManager->increase(this->scoreValue);
	this->getParent()->addObj(expl);
}

void UFO::draw()
{
	refreshTexture();
	glBindTexture(GL_TEXTURE_2D, texture[curTextureIndex-1]);
	float x_left = x_pos - WIDTH / 2;
	float x_right = x_pos + WIDTH / 2;
	float y_top = y_pos + HEIGHT / 2;
	float y_bot = y_pos - HEIGHT / 2;
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex2f(x_left, y_top);

	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(x_right, y_top);

	glTexCoord2f(1.0f, 0.0f);
	glVertex2f(x_right, y_bot);

	glTexCoord2f(0.0f, 0.0f);
	glVertex2f(x_left, y_bot);
	glEnd();
}

void UFO::update(bool status[])
{
	if (this->MARK_FOR_DELETE) {

		// make sure the object is deleted NEXT FRAME
		this->getParent()->removeObj(this);
		return;
	}
	y_pos -= speed;
	if (y_pos < bound_y_bot) {
		// prevent memory leak, delete a bullet when it go out of Screen
		this->MARK_FOR_DELETE = true;
		return;
	}
	fire();
}

void UFO::fire()
{
	clock_t cur = clock();
	clock_t duration = cur - this->last_fire;
	float duration_in_sec = (float)duration / (float)CLOCKS_PER_SEC;


	if (duration_in_sec > cooldown) {
		
		int decision = rand.random_integer(0, 100); 

		last_fire = cur;
		LaserShot* shot;
		if (decision<50)
			 shot = new LaserShot(x_pos - WIDTH/3, y_pos - 60, TAG_ENEMY_ATK);
		else 
			shot = new LaserShot(x_pos + WIDTH/3, y_pos - 60, TAG_ENEMY_ATK);

		shot->setTexture(this->tm->play_texture, 15);
		Scene* parent = this->getParent();
		parent->addObj(shot);
		soundManager->playMissileLaunch();
	}
}



float UFO::getXpos()
{
	return this->x_pos;
}

float UFO::getYpos()
{
	return this->y_pos;
}

float UFO::getH()
{
	return this->HEIGHT;
}

void UFO::onCollision(GameObject * other)
{
	if (other->tag == TAG_PLAYER_ATK) {
		//TODO: set an explosion
		explosion();
		this->MARK_FOR_DELETE = true;
	}
}

float UFO::getW()
{
	return this->WIDTH;
}