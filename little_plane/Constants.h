#pragma once

static int windows_width = 500;
static int windows_height = 500;
static const int MAX_STATUS = 100;

static const int DIRECTION_LEFT = 1;
static const int DIRECTION_RIGHT = 2;
static const int DIRECTION_UP = 3;
static const int DIRECTION_DOWN = 4;
static const int DIRECTION_PLUS = 5;
static const int DIRECTION_MINUS = 6;
static const int FIRE = 7;
static const int SPACEBAR = 8;
static const int ENTER = 9;

static const int MENU_SCENE = 0;
static const int PLAY_SCENE = 1;
static const int END_SCENE = 2;


static int MENU_TOP_Y = 250;
static int MENU_BOT_Y = 150;


static int UNTAG = 0;
static int TAG_PLAYER = 1;
static int TAG_PLAYER_ATK = 2;
static int TAG_ENEMY = 3;
static int TAG_ENEMY_ATK = 4;