/*
Thai Thien
1351040

*/

#include <GL\glew.h>
#include<GL\freeglut.h>

#include "Constants.h"

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

#include "PlayerPlane.h"
#include "Scene.h"

#include "SoundManager.h"
#include "ArtWord.h"
#include "MenuChooser.h"
#include "SceneManager.h"
#include "UFO.h"
#include "Spawmer.h"
#include "TextureManager.h"
#include "ScoreBoard.h"
#include "SceneFactory.h"

using namespace std;


void keyboard(unsigned char key, int x, int y);
void specialKeyboardInput(int key, int x, int y);
int cur_x;
int cur_y;
int begin_x;
int begin_y;
int is_mouse_down;
bool is_draw = false;
bool is_fill = false;


PlayerPlane*  player;


ArtWord* startWord;
ArtWord* exitWord;
ArtWord* retryWord;
MenuChooser* menuChooser;
Spawmer* spawmer;


Scene scene_list[3];
Scene* current_scene;


//sound
SoundManager* soundManager;
SceneManager* sceneManager;
TextureManager* textureManager;
ScoreManager* scoreManager;
ScoreBoard* playScoreBoard;
ScoreBoard* endScoreBoard;




void display(void)
{
	/* clear all pixels */
	glClear(GL_COLOR_BUFFER_BIT);

	// render current scene
	current_scene->draw();
	// glFlush();

	
	///

	glutSwapBuffers(); // for double buffer
}


void init(void)
{

	textureManager = new TextureManager();
	soundManager = new SoundManager();
	sceneManager = new SceneManager();
	scoreManager = new ScoreManager();
	MgList* mgList = new MgList(soundManager, sceneManager, textureManager, scoreManager);
	sceneManager->setSceneList(scene_list, mgList);

	//set is mouse down = 0
	is_mouse_down = 0;

	/* select clearing color */
	glClearColor(0, 0, 0, 0.0);

	/* initialize viewing values */
	glMatrixMode(GL_PROJECTION);
	
	glLoadIdentity();

	//glOrtho(0.0, 30.0, 0.0, 35.0, -1.0, 1.0);
	gluOrtho2D(0, windows_width, 0, windows_height);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	// INIT ALL SCENE AND PICK START SCENE HERE
	
	SceneFactory::createMenuScene(scene_list,mgList);
	SceneFactory::createEndScene(scene_list, mgList);
	SceneFactory::createPlayScene(scene_list, mgList);

	current_scene = &scene_list[MENU_SCENE]; // set PLAY_SCENE as current scene

	std::cout << "init \n";
}

void mouseFunc(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {

		}
	}
}

void idleFunction() {
	// game loop here
	int active_scene_code = sceneManager->getActiveScene();
	current_scene = &scene_list[active_scene_code];
	current_scene->gameLoop();
}

void ActivMouseMove(int x, int y)
{
	//Chi cap nhat thong tin khi chuot trai duoc nhan keo
	//Ham activMouseMove cung duoc goi khi chuot phai duoc nhan keo

	cur_x = x;
	cur_y = windows_height - y;

	glutPostRedisplay();
}


int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	//glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);


	glutInitWindowSize(windows_width, windows_height);

	glutInitWindowPosition(100, 100);
	glutInitContextVersion(1, 5);
	glutCreateWindow("Little Plane");


	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	init();

	glutIdleFunc(idleFunction);
	glutDisplayFunc(display);
	glutMouseFunc(mouseFunc);
	glutKeyboardFunc(keyboard);//(15)
	glutSpecialFunc(specialKeyboardInput);
	glutMotionFunc(ActivMouseMove);
	glutMainLoop();



	return 0; /* ANSI C requires main to return an int. */

}




void specialKeyboardInput(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_UP:
		//std::cout << "UP \n";
		current_scene->setStatus(DIRECTION_UP);
		break;

	case GLUT_KEY_DOWN:
		//std::cout << "DOWN \n";
		current_scene->setStatus(DIRECTION_DOWN);
		break;

	case GLUT_KEY_LEFT:
		//std::cout << "LEFT \n";
		current_scene->setStatus(DIRECTION_LEFT);
		break;

	case GLUT_KEY_RIGHT:
		//std::cout << "RIGHT \n";
		current_scene->setStatus(DIRECTION_RIGHT);
		break;

	


	default:
		break;
	}
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {

	case 13: { //Enter

		current_scene->setStatus(ENTER);
		break;
	}

	case 'f': {
		// f to fire
		current_scene->setStatus(FIRE);
		break;
	}

	case '+': {
		std::cout << "plus\n";
		break;
	}
	case '-': {
		std::cout << "minus\n";
		break;
	}

	case 'l': {
		std::cout << "L  \n";
		break;
	}

	case 'r': {
		std::cout << "R \n";
		break;
	}


	case 27: //ESC
		exit(0);
		break;

	default:
		break;
	}
	
}