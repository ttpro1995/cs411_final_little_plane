#include "Spawmer.h"
#include "Constants.h"

#include "Scene.h"


Spawmer::Spawmer()
{
	this->x_pos = 250;
	this->y_pos = 400;
	this->name = "Spawmer";
	this->tag = UNTAG;
	this->HEIGHT = 0;
	this->WIDTH = 0;
}




Spawmer::~Spawmer()
{
}

GameObject* Spawmer::spawm(int x, int y)
{
	UFO* ufo = new UFO(x, y);
	ufo->setTexture(this->tm->enemy_ufo_texture, 5);
	ufo->setTextureManager(this->tm);
	ufo->setSoundManager(this->soundManager);
	ufo->setScoreManager(this->scoreManager);
	return ufo;
}


void Spawmer::update(bool status[])
{
	clock_t cur = clock();
	clock_t duration = cur - this->last_fire;
	float duration_in_sec = (float)duration / (float)CLOCKS_PER_SEC;

	
	if (duration_in_sec > cooldown) {
		last_fire = cur;
		int x_spawm = rand.random_integer(50, 450);
		GameObject* enemy = spawm(x_spawm, this->y_pos);
		Scene* parent = this->getParent();
		parent->addObj(enemy);
	}
}
