#pragma once
#include <SFML/Audio.hpp>

// sound are keeped here
// Sound can be play from here
class SoundManager
{
private:
	sf::SoundBuffer missile_launch_buff;
	sf::Sound fire_sound;

	sf::SoundBuffer explosion_buff;
	sf::Sound explosion_sound;

	sf::SoundBuffer blip_buff;
	sf::Sound blip_sound;

	sf::SoundBuffer select_buff;
	sf::Sound select_sound;

	sf::SoundBuffer playscene_background_buff;
	sf::Sound playscene_background_sound;

public:
	SoundManager();
	void playMissileLaunch();
	void playExplosion();
	void playBlip();
	void playSelect();
	void playPlaySceneBackground();
	void stopPlaySceneBackground();
};

