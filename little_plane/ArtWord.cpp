#include "ArtWord.h"






ArtWord::ArtWord(int x, int y, int W, int H)
{
	this->x_pos = x;
	this->y_pos = y;
	this->HEIGHT = H/5;
	this->WIDTH = W/5;
}

ArtWord::~ArtWord()
{
}

void ArtWord::draw()
{
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	float x_left = x_pos - WIDTH / 2;
	float x_right = x_pos + WIDTH / 2;
	float y_top = y_pos + HEIGHT / 2;
	float y_bot = y_pos - HEIGHT / 2;
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex2f(x_left, y_top);

	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(x_right, y_top);

	glTexCoord2f(1.0f, 0.0f);
	glVertex2f(x_right, y_bot);

	glTexCoord2f(0.0f, 0.0f);
	glVertex2f(x_left, y_bot);
	glEnd();
}

void ArtWord::update(bool status[])
{
}

float ArtWord::getXpos()
{
	return this->x_pos;
}
float ArtWord::getYpos()
{
	return this->y_pos;
}

float ArtWord::getH()
{
	return this->HEIGHT;
}

float ArtWord::getW()
{
	return this->WIDTH;
}
