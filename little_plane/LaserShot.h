#pragma once
#include "GameObject.h"
#include "Constants.h"

// something for player and UFO to fire at each other
class LaserShot :
	public GameObject
{
private:
	// pos should be center of the plane 
	float speed;

	// set up a boundary for lasershot to destroy the shot
	float bound_x_left = WIDTH / 2;
	float bound_x_right = windows_width - WIDTH / 2;
	float bound_y_top = windows_height  - HEIGHT / 2;
	float bound_y_bot = 0 + HEIGHT;
public:
	LaserShot(int x, int y, int Tag);
	~LaserShot();
	virtual  void draw() override;
	virtual  void update(bool status[]) override;


	virtual  float getXpos() override;
	virtual  float getYpos() override;
	virtual  float getW() override;
	virtual  float getH() override;

	virtual void onCollision(GameObject* other) override;
};

