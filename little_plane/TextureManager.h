#pragma once
#include <GL\freeglut.h>

// all texture are loaded and store here
class TextureManager
{
private:
	#define NUM_PLAY_TEXTURES 7	
	char* play_texture_name[NUM_PLAY_TEXTURES] = { "texture/play_scene/Plane.png", "texture/play_scene/Plane_1.png", "texture/play_scene/Plane_2.png", "texture/play_scene/Plane_3.png","texture/play_scene/missile1.png","texture/play_scene/missile2.png","texture/play_scene/missile3.png" };
	char * menu_artword_name[4] = { "texture/menu_scene/word_exit.png","texture/menu_scene/word_start.png","texture/menu_scene/word_retry.png","texture/menu_scene/title.png" };
	char * chooser_texture_name[3] = { "texture/menu_scene/dot_chooser1.png","texture/menu_scene/dot_chooser2.png","texture/menu_scene/dot_chooser3.png" };
	char* enemy_ufo_name[3] = { "texture/play_scene/ufo1.png", "texture/play_scene/ufo2.png", "texture/play_scene/ufo3.png" };
	char* explosion_name[3] = { "texture/play_scene/explosion1.png", "texture/play_scene/explosion2.png", "texture/play_scene/explosion3.png" };

public:
	GLuint play_texture[NUM_PLAY_TEXTURES];
	GLuint start_artword_texture[1];
	GLuint exit_artword_texture[1];
	GLuint retry_artword_texture[1];
	GLuint title_artword_texture[1];
	GLuint chooser_texture[3];
	GLuint enemy_ufo_texture[3];
	GLuint explosion_texture[3];
	TextureManager();
	~TextureManager();
};

