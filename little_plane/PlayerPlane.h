#pragma once
#include <GL\freeglut.h>
#include "Constants.h"
#include "GameObject.h"



class PlayerPlane : public GameObject
{
private:
	// pos should be center of the plane 

	float speed = 8;

	// set up a boundary for plane. Plane cannot go out this bound
	float bound_x_left = WIDTH/2;
	float bound_x_right = windows_width - WIDTH / 2;
	float bound_y_top = windows_height - 100 - HEIGHT / 2;
	float bound_y_bot = 0 + HEIGHT;
	
	float last_fire = 0;
	float cooldown = 0.5;
	
	// when player is detroyed, loseFlag set to true
	bool loseFlag = false;
	
	// when loseFlag is true, the cooldown is start
	// it -1 every frame until it reach 0
	// then show lose scene with score
	int loseCD = 60;
public:
	PlayerPlane();
	virtual  void draw() override;
	~PlayerPlane();
	virtual  void update(bool status[]) override;
	void movement(int direction) ;
	void fire();

	virtual  float getXpos() override;
	virtual  float getYpos() override;
	virtual  float getW() override;
	virtual  float getH() override;
	void explosion();
	virtual void onCollision(GameObject* other) override;
};

