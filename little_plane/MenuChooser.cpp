#include "MenuChooser.h"
#include "Constants.h"
#include <iostream>
using namespace std;

MenuChooser::MenuChooser(float x1, float y1, float x2, float y2)
{
	this->x_pos1 = x1;
	this->y_pos1 = y1;
	this->x_pos2 = x2;
	this->y_pos2 = y2;
	this->x_pos = x1;
	this->y_pos = y1;
	this->HEIGHT = 50;
	this->WIDTH = 50;
	this->choose = START;
}


MenuChooser::~MenuChooser()
{
}

void MenuChooser::draw()
{
	refreshTexture();
	glBindTexture(GL_TEXTURE_2D, texture[curTextureIndex-1]);
	float x_left = x_pos - WIDTH / 2;
	float x_right = x_pos + WIDTH / 2;
	float y_top = y_pos + HEIGHT / 2;
	float y_bot = y_pos - HEIGHT / 2;
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex2f(x_left, y_top);

	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(x_right, y_top);

	glTexCoord2f(1.0f, 0.0f);
	glVertex2f(x_right, y_bot);

	glTexCoord2f(0.0f, 0.0f);
	glVertex2f(x_left, y_bot);
	glEnd();
}

void MenuChooser::update(bool status[])
{
	if (status[DIRECTION_UP]) {
		choose = START;
		this->x_pos = x_pos1;
		this->y_pos = y_pos1;
		soundManager->playBlip();
	}
	else if (status[DIRECTION_DOWN]) {
		choose = EXIT;
		this->x_pos = x_pos2;
		this->y_pos = y_pos2;
		soundManager->playBlip();
	}

	if (status[ENTER]) {
		soundManager->playSelect();
		if (choose == START)
			chooseStart();
		if (choose == EXIT)
			chooseExit();
	}

	
}

void MenuChooser::chooseStart()
{
	this->sceneManager->resetGame();
	this->sceneManager->loadScene(PLAY_SCENE);
	
}

void MenuChooser::chooseExit()
{
	this->sceneManager->quitApplication();
}


float MenuChooser::getXpos()
{
	return this->x_pos;
}

float MenuChooser::getYpos()
{
	return this->y_pos;
}

float MenuChooser::getH()
{
	return this->HEIGHT;
}

float MenuChooser::getW()
{
	return this->WIDTH;
}
