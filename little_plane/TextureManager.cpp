#include "TextureManager.h"
#include "helper.h"


TextureManager::TextureManager()
{
	int* width = new int(80);
	int* height = new int(80);
	for (int i = 0; i < NUM_PLAY_TEXTURES; i++) {
		play_texture[i] = png_texture_load(play_texture_name[i], width, height);
	}
	start_artword_texture[0] = png_texture_load(menu_artword_name[1], new int(0), new int(0));
	exit_artword_texture[0] = png_texture_load(menu_artword_name[0], new int(0), new int(0));
	retry_artword_texture[0] = png_texture_load(menu_artword_name[2], new int(0), new int(0));
	title_artword_texture[0] = png_texture_load(menu_artword_name[3], new int(0), new int(0));
	for (int i = 0; i < 3; i++) {
		chooser_texture[i] = png_texture_load(chooser_texture_name[i], width, height);
	}

	for (int i = 0; i < 3; i++) {
		enemy_ufo_texture[i] = png_texture_load(enemy_ufo_name[i], width, height);
	}

	for (int i = 0; i < 3; i++) {
		explosion_texture[i] = png_texture_load(explosion_name[i], width, height);
	}

	
}


TextureManager::~TextureManager()
{
}
