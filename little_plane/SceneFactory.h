#pragma once
#include "Scene.h"
#include "PlayerPlane.h"


#include "SoundManager.h"
#include "ArtWord.h"
#include "MenuChooser.h"
#include "SceneManager.h"
#include "UFO.h"
#include "Spawmer.h"
#include "TextureManager.h"
#include "ScoreBoard.h"

struct MgList {
	SoundManager* soundManager;
	SceneManager* sceneManager;
	TextureManager* textureManager;
	ScoreManager* scoreManager;
	MgList(SoundManager* soundMg, SceneManager* sceneMg, TextureManager* textureMg, ScoreManager* scoreMg) {
		this->soundManager = soundMg;
		this->sceneManager = sceneMg;
		this->scoreManager = scoreMg;
		this->textureManager = textureMg;
	}
};

// Factory that put the objects into scene
class SceneFactory {
public:
	SceneFactory();

	static void createMenuScene(Scene* sceneList,MgList* mg );
	static void createPlayScene(Scene* sceneList, MgList* mg);
	static void createEndScene(Scene* sceneList, MgList* mg);
};